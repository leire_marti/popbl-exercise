/**This class is part of the JWanki project, and is meant to help big and
trafficked cities to manage their parking problems. The garage manages the traffic
of a paying garage by registering users of the garage, their cars, and taking 
track of the cars' flow. There are also employees on the garage. The garage also 
offers different price depending on the user's criteria. Each user could park daily
just for a specified amount of time. The users could be santioned if they passed 
the time they could be parked.*/

/**This class contains two different functions that search for all the users who
have not payed their santion bill and prints a list of them; or a specified user, 
defined when calling at the function, and also retutning a list. The function 
searches on the database by the SQL method.*/


package bDatos;
import java.sql.ResultSet;

public class Consultas {

	public void multasSinPagar() {
		ResultSet myRs = null;
		myRs = myStmt.executeQuery("select nombre, multa from employees"
				+ "join multas on multas.usuarioID = usuario.usuarioID"
				+ " where fecha_pagado is null");
		
		
		
		while (myRs.next()) {
			System.out.println(myRs.getString("last_name") + ", " + myRs.getString("first_name"));
		}


	}
	public void multasSinPagarUsuario(String id) {
		ResultSet myRs = null;
		myRs = myStmt.executeQuery("select nombre, multa from employees"
				+ "join multas on multas.usuarioID = usuario.usuarioID"
				+ " where (fecha_pagado is null) and (usuarioID="+id+"';");
		
		
		
		while (myRs.next()) {
			System.out.println(myRs.getString("last_name") + ", " + myRs.getString("first_name"));
		}


	}
}


/**This class was created by Manex Bengoa Arregi, Inge Ruiz de Azua, 
Leire Martinez de Santos and Josu Urrutia for the JWanki project in 2018,
in Arrasate, Euskal Herria.*/