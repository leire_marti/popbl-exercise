/**This class is part of the JWanki project, and is meant to help big and
trafficked cities to manage their parking problems. The garage manages the traffic
of a paying garage by registering users of the garage, their cars, and taking 
track of the cars' flow. There are also employees on the garage. The garage also 
offers different price depending on the user's criteria. Each user could park daily
just for a specified amount of time. The users could be santioned if they passed 
the time they could be parked.*/

/**This class holds four different functions that deletes a specified Tarifa, 
Usuario, Empleado or Coche by their ID. The function searches on the database 
by the SQL method.*/


package bDatos;

public class Delete {
	
	public void deleteTarifa(String id) {		
		
		String sql = "delete from Tarifa where id='"+id+"'";
			int rowsAffected = myStmt.executeUpdate(sql);
			System.out.println("Rows affected: " + rowsAffected);


	}
	public void deleteUsuario(String id) {
	
		String sql = "delete from Usuario where id='"+id+"'";
			int rowsAffected = myStmt.executeUpdate(sql);
			System.out.println("Rows affected: " + rowsAffected);

}
	public void deleteEmpleado(String id) {
	
		String sql = "delete from Empleado where id='"+id+"'";
				int rowsAffected = myStmt.executeUpdate(sql);
				System.out.println("Rows affected: " + rowsAffected);

}
	public void deleteCoche(String id) {

		String sql = "delete from Coche where id='"+id+"'";
			int rowsAffected = myStmt.executeUpdate(sql);
			System.out.println("Rows affected: " + rowsAffected);

}

}

/**This class was created by Manex Bengoa Arregi, Inge Ruiz de Azua, 
Leire Martinez de Santos and Josu Urrutia for the JWanki project in 2018,
in Arrasate, Euskal Herria.*/