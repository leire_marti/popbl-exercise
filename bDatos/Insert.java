/**This class is part of the JWanki project, and is meant to help big and
trafficked cities to manage their parking problems. The garage manages the traffic
of a paying garage by registering users of the garage, their cars, and taking 
track of the cars' flow. There are also employees on the garage. The garage also 
offers different price depending on the user's criteria. Each user could park daily
just for a specified amount of time. The users could be santioned if they passed 
the time they could be parked.*/

/**This class contains four different functions that create the prior mentioned 
fields: Prize, User, Employer and Car (Tarifa, Usuario, Empleado or Coche). The 
function searches on the database by the SQL method.*/


package bDatos;

public class Insert {

  /**The fields required to create a Tarifa are: an ID, a description and a prize*/
	
	public void insertTarifa(int tarifaID, String descripcion,int precio) {
				
			String sql = "insert into tarifa " + " (tarifaID, descripcion, precio)"
				+ " values ('"+tarifaID+"', '"+descripcion+"', '"+precio+"')";
				myStmt.executeUpdate(sql);

	}

  /**The fields required to create a User are: an ID, a DNI, a name, a surname,
     a bank account, an address and a user name.*/

	public void insertUsuario(int usuarioID, String DNI,String nombre,
		String apellido,String CC,String direccion,String nombreUsuario) {
		
			String sql = "insert into usuario " + " (usuarioID, DNI, nombre, apellido,
			CC, direccion,
			nombreUsuario"+ "contraseņa,foto,tarifaID)"
			+ " values ('"+usuarioID+"', '"+DNI+"', '"+nombre+"', '"
			+apellido+"', '"+CC+"', '"+direccion+
			"', '"+nombreUsuario+"')";
			myStmt.executeUpdate(sql);

	}

  /**The fields required to create a Employee are: an ID, a DNI, a name, a surname, 
     an address and a user name.*/

	public void insertEmpleado(int usuarioID, String DNI,String nombre,
		String apellido,String direccion,String nombreUsuario) {
		
			String sql = "insert into usuario " + " (usuarioID, DNI, nombre, apellido,
			direccion,nombreUsuario"+ "contraseņa,foto,tarifaID)"
			+ " values ('"+usuarioID+"', '"+DNI+"', '"+nombre+"', '"
			+apellido+", '"+direccion+
			"', '"+nombreUsuario+"')";
			myStmt.executeUpdate(sql);

	}

  /**The fields required to create a Car are: an ID, a license plate number, the car model 
     and the username of the owner*/

	public void insertCoche(int cocheID, String matricula,String modelo,
		String nombreUsuario) {
		
			String sql = "insert into usuario " + " (cocheID, matricula, modelo, UsuarioID)"
			+ " values ('"+cocheID+"', '"+matricula+"', '"+modelo+"', '"+nombreUsuario+
			"')";
			myStmt.executeUpdate(sql);

	}

}

/**This class was created by Manex Bengoa Arregi, Inge Ruiz de Azua, 
Leire Martinez de Santos and Josu Urrutia for the JWanki project in 2018,
in Arrasate, Euskal Herria.*/